/*
 * t_timer.c
 *
 *  Created on: Sep 21, 2012
 *      Author: peter96
 */


#include "gadget.h"
#include <time.h>
#include <pthread.h>


int t_timer_create(int args_num, int *args)
{
	int status = SUCCESS;
	timer_t timerid;

	status = timer_create(CLOCK_REALTIME, NULL, &timerid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	return status;
}

int t_clock_getres(int args_num, int *args)
{
	struct timespec res;
	int status = SUCCESS;

	status = clock_getres(CLOCK_REALTIME, &res);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	if(*args != res.tv_nsec)
	{
		_assert(__func__,__LINE__);
		printk("res.tv_nsec = %d\n",res.tv_nsec);
		return ERROR;
	}

	return status;
}
