/*
 * t_sem.c
 *
 *  Created on: Sep 20, 2012
 *      Author: peter96
 */

#include "gadget.h"
#include <semaphore.h>
#include <pthread.h>


int t_sem_init(int args_num, int *args)
{
	int status = SUCCESS;
	sem_t semid;

	status = sem_init(&semid, NULL, NULL);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = sem_destroy(&semid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	return status;
}


pthread_t	pid[2];
sem_t semid;

void f_sem_wait(void *args)
{
	int status = SUCCESS;


	status = sem_post(&semid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	sleep(10);

	status = sem_wait(&semid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}


	pthread_exit(1);
	return status;
}

void f_sem_post(void *args)
{
	int status = SUCCESS;

	status = sem_wait(&semid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	sleep(10);

	status = sem_post(&semid);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	pthread_exit(1);
}

int t_sem_wait(int args_num, int *args)
{
	int status = SUCCESS;
	pthread_attr_t attr;


	status = pthread_attr_init(&attr);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}


	status = sem_init(&semid, NULL, NULL);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = pthread_create(&pid[0], &attr, f_sem_wait, NULL);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = pthread_create(&pid[1], &attr, f_sem_post, NULL);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	return status;

}
