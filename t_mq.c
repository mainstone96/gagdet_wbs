/*
 * t_sample.c
 *
 *  Created on: Sep 20, 2012
 *      Author: peter96
 */
#include "gadget.h"
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#define MQ_NAME "/posix"
#define MAX_PATH 128

int t_mq_open_close(int args_num, int *args)
{
	int status = SUCCESS;
	mqd_t msgq;

	msgq = mq_open(MQ_NAME, O_RDWR | O_CREAT | O_EXCL, S_IRWXU | S_IRWXG, NULL);
	if(msgq == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = mq_close(msgq);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	return status;
}


int t_mq_send(int args_num, int *args)
{
	mqd_t msgq;
	char buff[]="send message by message queue";
	int status = SUCCESS;
	unsigned int prio = _POSIX_MQ_PRIO_MAX -1;


	msgq = mq_open(MQ_NAME, O_RDWR);
	if(msgq == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

//	for(prio = 0; prio < _POSIX_MQ_PRIO_MAX-1; prio++)
//	{
		//printk("mq_send : %d \n",prio);
		status = mq_send(msgq,buff, strlen(buff)+1, prio);
		if(status == ERROR)
		{
			_assert(__func__,__LINE__);
			return ERROR;
		}
//	}


	status = mq_close(msgq);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	return status;
}

int t_mq_recive(int args_num, int *args)
{
	int status = SUCCESS;
	char buff[MAX_PATH]="";
	mqd_t msgq;
	struct mq_attr attr;
	ssize_t	msgs;
	unsigned int prio = _POSIX_MQ_PRIO_MAX -1;

	msgq = mq_open(MQ_NAME, O_RDWR);
	if(msgq == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = mq_getattr(msgq, &attr);
	if(msgq == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

#if _DEBUG
	printk("Queue :\n\t- stores at most %ld messages\n\t- large at most %ld bytes each\n\t- currently holds %ld messages\n",
			attr.mq_maxmsg, attr.mq_msgsize, attr.mq_curmsgs);
#endif

	msgs = mq_receive(msgq, buff, MAX_PATH, &prio);
	if(msgs == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

#if _DEBUG
	printk("%s : message (%d bytes) from %d: %s\n", __func__, msgs, prio, buff);
#endif

	status = mq_close(msgq);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}
	return status;
}


int t_mq_unlink(int args_num, int *args)
{
	int status = SUCCESS;
	struct mq_attr wanted_attrs, actual_attrs;
	mqd_t msgq;

	/* filling the attribute structure */
	wanted_attrs.mq_flags = 0;                    /* no exceptional behavior (just O_NONBLOCK currently available)  */
	wanted_attrs.mq_maxmsg = 100;                 /* room for at most 100 messages in the queue */
	wanted_attrs.mq_msgsize = MAX_PATH;      /* maximum size of a message */
	wanted_attrs.mq_curmsgs = 123;                /* this (current number of messages) will be ignored */

	msgq = mq_open(MQ_NAME, O_RDWR);
	if(msgq == (mqd_t)ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	status = mq_getattr(msgq, &actual_attrs);
	if(msgq == (mqd_t)ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

	/* building the structure again for modifying the existent queue */
	wanted_attrs.mq_flags = O_NONBLOCK;
	wanted_attrs.mq_maxmsg = 350;               /* this will be ignored by mq_setattr() */
	wanted_attrs.mq_msgsize = MAX_PATH;    /* this will be ignored by mq_setattr() */
	wanted_attrs.mq_curmsgs = 123;              /* this will be ignored by mq_setattr() */

	/* trying to later set different attributes on the queue        --  mq_setattr()    */
	status = mq_setattr(msgq, &wanted_attrs, NULL);
	if(msgq == (mqd_t)ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

#if _DEBUG
    /* getting queue attributes after creation */
	status = mq_getattr(msgq, &actual_attrs);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

    printk("setattr():\n\t- non blocking flag: %d\n\t- maximum number of messages: %ld\n\t- maximum size of a message: %ld\n\t- current number of messages: %ld\n",
    		(actual_attrs.mq_flags == 0 ? 0 : 1), actual_attrs.mq_maxmsg, actual_attrs.mq_msgsize, actual_attrs.mq_curmsgs);
#endif

	status = mq_close(msgq);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

    status = mq_unlink(MQ_NAME);
	if(status == ERROR)
	{
		_assert(__func__,__LINE__);
		return ERROR;
	}

    return status;
}
