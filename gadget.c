/*
 * posix.c
 *
 *  Created on: Sep 20, 2012
 *      Author: peter96
 */
#include "gadget.h"

/*
 *  declaration of functions
 */
extern int t_sample(int args_num, int *args);

// message queue
extern int t_mq_open_close(int args_num, int *args);
extern int t_mq_send(int args_num, int *args);
extern int t_mq_recive(int args_num, int *args);
extern int t_mq_unlink(int args_num, int *args);

// semaphore
extern int t_sem_init(int args_num, int *args);
extern int t_sem_wait(int args_num, int *args);

// timer
int t_timer_create(int args_num, int *args);
int t_clock_getres(int args_num, int *args);

static tCase tList[] = {
	{"mq_open", t_mq_open_close, 0, 0, SUCCESS},
	{"mq_send", t_mq_send, 0, 0, SUCCESS},
	{"mq_recive", t_mq_recive, 0, 0, SUCCESS},
	{"mq_open2", t_mq_open_close, 0, 0, ERROR},
	{"mq_send", t_mq_send, 0, 0, SUCCESS},
	{"mq_recive", t_mq_recive, 0, 0, SUCCESS},
	{"mq_unlink", t_mq_unlink, 0, 0, SUCCESS},
	{"mq_open", t_mq_open_close, 0, 0, SUCCESS},
	{"mq_send", t_mq_send, 0, 0, SUCCESS},
	{"mq_unlink", t_mq_unlink, 0, 0, SUCCESS},
//	{"sem_int", t_sem_init, 0, 0, SUCCESS},
//	{"sem_wait", t_sem_wait	, 0, 0, SUCCESS},
	{"timer_create", t_timer_create	, 0, 0, SUCCESS},
	{"clock_getres", t_clock_getres, 1, 10000000, SUCCESS},
};



int main()
{
	int status = SUCCESS;
	int index = 0;
	int nPass = 0, nFail = 0;


	printk("Test Case : %d \n", ARRAY_SIZE(tList));

	for(index = 0; index < ARRAY_SIZE(tList); index++ )
	{

		status = tList[index].fp(tList[index].args_num, tList[index].args);
		if(status == tList[index].nExpect)
		{
			nPass++;
			printk("%s : pass \n",tList[index].tName);
		}
		else
		{
			nFail++;
			printk("%s : fail (expect:%d, fact %d , errno %d) \n",tList[index].tName,
					tList[index].nExpect,status, errno);
		}
	}

	printk("\n TOTAL : %d / PASS : %d / FAIL : %d \n",ARRAY_SIZE(tList), nPass, nFail);
	return 0;
}



