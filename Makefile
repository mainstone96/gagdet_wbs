## Project makefile
PRJ_NAME = simpleT

## BSP selection
BSP   = cots8560v2
ARCH  = powerpc
CPU   = mpc8560 
TOOLS = gnu

export BSP ARCH CPU TOOLS

## project configuration files
PRJ_USRINIT_SRC	= usrAppInit.c gadget.c t_sample.c t_mq.c t_sem.c t_timer.c

PRJ_EXTRA_CXX_OBJS = 

USER_INCLUDE += -I$(KERNEL_BASE)/include/vxworks

include $(KERNEL_BASE)/make/rules.kern.gnu

