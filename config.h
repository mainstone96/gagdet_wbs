/*
 * config.h - system configuration header
 *
 * Copyright(c) 2012 MDS Technology Co.,Ltd.
 * All rights reserved.
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_

/* kernel component configurations */
#undef  CONFIG_SYS_MMU_EARLY_INIT                       /* disable MMU early init */
#define CONFIG_BSP_EARLY_INIT                           /* enable early init */
#define CONFIG_BSP_CONSOLE_INIT                         /* enable BSP console */
#define CONFIG_SYS_EXC_INIT                             /* enable CPU exception init */
#define CONFIG_MMU                                      /* enable MMU init */
#define CONFIG_VMS                                      /* enable VMS init */
#define CONFIG_PROCESS                                  /* enable real-time process */
#define CONFIG_VMS_ELF_LOADER                           /* enable ELF type user app loader */
#define CONFIG_TIMER                                    /* enable software watchdog timer */
#define CONFIG_THREAD_HOOK                              /* enable thread hooks */
#define CONFIG_MESSAGE_QUEUE                            /* enable message queue */
#define CONFIG_MSGPORTLIB                               /* enable IPC */
#define CONFIG_SYSLOG                                   /* enable sysLog */
#define CONFIG_SYSLOG_MAX_LOGMSG		32              /* maximum sysLog entries */
#define CONFIG_SYMBOL_TABLE                             /* enable kernel symbol management */
#define CONFIG_FAULTLIB                                 /* enable fault monitoring */
#define CONFIG_KERNEL_MODULE                            /* enable kernel modules */
#define CONFIG_ELF_MODULE                               /* enable ELF type kernel module */

#define CONFIG_SYSCALL_THREADLIB                        /* enable syscall for thread API */
#define CONFIG_SYSCALL_SEMLIB                           /* enable syscall for semaphore API */
#define CONFIG_SYSCALL_MQLIB                            /* enable syscall for message queue API */
#define CONFIG_SYSCALL_MSGPORTLIB                       /* enable syscall for IPC API */
#define CONFIG_SYSCALL_IOSLIB                           /* enable syscall for common I/O API */
#define CONFIG_SYSCALL_MMANLIB							/* enable syscall for memory management API */


/* POSIX Library */
#define CONFIG_POSIX_LIBRARY
#ifdef CONFIG_POSIX_LIBRARY
#define CONFIG_POSIX_PTHREAD							/* enable pthread interface */
#define CONFIG_POSIX_SIGNAL								/* enable signal of POSIX */
#endif

/* BSP configurations */
#define CONFIG_BSP_CONSOLE_INIT
#define CONFIG_BSP_CONSOLE_BAUD_RATE	115200          /* BSP console speed */
#define CONFIG_IO_TTY_CONSOLE           "/dev/tty0"     /* console device path */
#define CONFIG_IO_TTY_NUM               1               /* number of TTY devices */

/* subsystem configurations */
#define CONFIG_IOSYS                                    /* enable I/O subsystem */

#ifdef  CONFIG_IOSYS
#define CONFIG_IOSYS_MAX_FILE			100             /* maximum number of file to handle */
#define CONFIG_IOSYS_MAX_DEVICE			100             /* maximum number of device driver to handle */
#define CONFIG_TERMIO                                   /* enable terminal I/O */

#define CONFIG_VFS_CORE                                 /* enable VFS */
#define SUBSYS_FS_MSDOSFS                               /* enable MS-DOS file system */
#define SUBSYS_FS_EXT2FS                               /* enable MS-DOS file system */


#define CONFIG_SHELL_CORE                               /* enable target shell */

#ifdef  CONFIG_SHELL_CORE
#define CONFIG_SYMBOL_SHOW                              /* enable symbol show command */
#define CONFIG_THREAD_SHOW                              /* enable thread show command */
#define CONFIG_SEM_SHOW                                 /* enable semaphore show command */
#define CONFIG_PROCESS_SHOW                             /* enable process show command */
#define CONFIG_SHELL_LOGO                               /* enable target shell logo */
#define CONFIG_SHELL_PROMPT                 "-> "       /* set default shell prompt */
#endif  /* !CONFIG_SHELL_CORE */

#undef CONFIG_SYS_LWIPNET                               /* enable lwIP network stack */
#if defined(CONFIG_SYS_LWIPNET)
#define LWIPNET_MAC                     "00:55:53:55:55:00"
#define LWIPNET_IP                      "192.168.10.123"
#define LWIPNET_SUBNET_MASK             "255.255.255.0"
#define LWIPNET_GATEWAY                 "192.168.10.1"
#endif

#if defined(CONFIG_SYS_LWIPNET) || defined(CONFIG_SYS_BSDNET)

#define CONFIG_SYSCALL_EVTPROLIB                        /* enable syscall for event profiler */
#define CONFIG_SYS_TARGET_SERVICE                       /* enable target service */
#define CONFIG_SYS_PROFILING_SERVICE                    /* enable profiler service */
#define CONFIG_SYS_MONITORING_SERVICE                   /* enable monitoring service */
#define CONFIG_SYS_DEBUG_SERVICE                        /* enable debug service */
#endif /* !CONFIG_SYS_LWIPNET || !CONFIG_SYS_BSD */

#ifdef CONFIG_SYS_TARGET_SERVICE
#define TAGT_COMMUNICATION_PATH_CTRL   9990             /* port number of target service */
#endif /* !CONFIG_SYS_TARGET_SERVICE */

#ifdef CONFIG_SYS_PROFILING_SERVICE
#define PROF_COMMUNICATION_PATH_CTRL   9995             /* port number of profiler service's control path */
#define PROF_COMMUNICATION_PATH_DATA   9996             /* port number of profiler service's data path */
#endif /* !CONFIG_SYS_PROFILING_SERVICE */

#ifdef CONFIG_SYS_MONITORING_SERVICE
#define MON_COMMUNICATION_PATH_CTRL    9991             /* port number of monitoring service */
#endif /* !CONFIG_SYS_MONITORING_SERVICE */

#endif	/* !CONFIG_IOSYS */

#define CONFIG_SYS_CACHE_SUPPORT
#define CONFIG_ICACHE_MODE          0x01
#define CONFIG_DCACHE_MODE          0x01

#define CONFIG_CPLUS_RUNTIME

#endif /*_CONFIG_H_ */
