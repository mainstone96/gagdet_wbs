/*
 * usrAppInit.c - User entry point
 */

#include <kernel.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
/* Local constants */

#define TOD_YEAR_BASE    1900

static void
setDate(void)
{
    struct tm      tmv;
    struct timeval tv;
    time_t         timevalue;
    uint           d = 20090113, t = 121212;


    if (gettimeofday(&tv, NULL) == ERROR) {
        sysLogOutMsg("%s: gettimeofday failed.\n");
        return;
    }


    localtime_r(&tv.tv_sec, &tmv);

    if (d > 0) {
        tmv.tm_year = (d / 10000) - TOD_YEAR_BASE;
        tmv.tm_mon  = (d % 10000)/100 - 1;
        tmv.tm_mday = d % 100;
        if (t > 0) {
            tmv.tm_hour = t / 10000;
            tmv.tm_min  = (t % 10000) / 100;
            tmv.tm_sec  = t % 100;
        }
    }

    tv.tv_sec  = mktime(&tmv);
    tv.tv_usec = 0;

    settimeofday(&tv, 0);
    timevalue = time(NULL);
    sysLogOutMsg("%s", ctime(&timevalue));

}
/**
 * usrAppInit - user initialization
 * 
 * This routine is entry point of user in kernel mode.
 */

void
usrAppInit(void)
{
	sysLogOutMsg("start user application!!!\n");

	main();

}

