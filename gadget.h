/*
 * simpleT.h
 *
 *  Created on: Sep 20, 2012
 *      Author: peter96
 */

#include <kernel.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_PATH 25

#define printk sysLogOutMsg

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(*array))

#define _assert(name,line) printk("assert->%s %d\n",name,line)
/*
 *  test case function
 */
typedef struct
{
	char tName[MAX_PATH];
	int (*fp)(int, int *);
	int args_num;
	int *args;
	int nExpect;
} tCase;


extern void _setLine(int *, int);
